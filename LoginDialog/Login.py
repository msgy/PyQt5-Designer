# coding:utf-8

from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtGui import QMouseEvent
from PyQt5 import Qt

import ui_login


class LoginDialog(QMainWindow, ui_login.Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.setWindowFlag(Qt.Qt.FramelessWindowHint)
        self.mDragWindow = False
        self.mMousePoint = []
        self.mBtnCancel.clicked.connect(self.onCancleClicked)

    def mouseMoveEvent(self, event):
        e = QMouseEvent(event)
        if self.mDragWindow:
            self.move(e.globalPos() - self.mMousePoint)
            e.accept()
            
    def mousePressEvent(self, event):
        e = QMouseEvent(event)
        if e.button() == Qt.Qt.LeftButton:
            self.mMousePoint = e.globalPos() - self.pos()
            self.mDragWindow = True
            e.accept()
        
    def mouseReleaseEvent(self, event):
        self.mDragWindow = False

    def onCancleClicked(self):
        self.close()